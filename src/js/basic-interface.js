/**
 * Location - all html document
 * Function - smooth anchors
 */
$(document).ready(function() {
    $(".section-rizor-hovertrax-header-navbar-link").on("click", "a", function(event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1200);
    });

    $(".dropdown-menu-navbar-link").on("click", "a", function(event) {
        event.preventDefault();
        var id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1200);
    });
});

/**
 * Location - header
 * Function - navbar underline-hover switcher
 */
$('.section-rizor-hovertrax-header-navbar-link').mouseover(function() {
    $(this).find('img').css('display', 'block');
});

$('.section-rizor-hovertrax-header-navbar-link').mouseout(function() {
    $(this).find('img').css('display', 'none');
});

/**
 * Location - dropdown menu
 * Function - menu underline-hover switcher
 */
$('.dropdown-menu-navbar-link').mouseover(function() {
    $(this).find('img').css('display', 'block');
});

$('.dropdown-menu-navbar-link').mouseout(function() {
    $(this).find('img').css('display', 'none');
});

/**
 * Location - Section Advantages
 * Function - color switcher
 */
document.querySelector('.section-advantages-design-color-colorlist-choose-color-chs').classList.add('active');
document.querySelector('.section-advantages-design-color-colorlist-color-of-geroskuter-img').classList.add('active');

document.querySelectorAll('.section-advantages-design-color-colorlist-choose-color-chs').forEach(el => {
    el.addEventListener('click', selectFirstTabs);
});

function selectFirstTabs(event) {
    let target = event.target.dataset.target;

    document.querySelectorAll('.section-advantages-design-color-colorlist-choose-color-chs, .section-advantages-design-color-colorlist-color-of-geroskuter-img').forEach(el => {
        el.classList.remove('active');
    });

    event.target.classList.add('active');
    document.querySelector('.' + target).classList.add('active');

    document.querySelector('.active').innerHTML = '<div class="section-advantages-design-color-colorlist-choose-color-chs-pointer-vertical"><img src="build/img/Фигура 9@1X.png" alt=""></div>';
}