let gulp = require('gulp');
let watch = require('gulp-watch');
let sass = require('gulp-sass');
let browserSync = require('browser-sync').create();
let autoprefixer = require('gulp-autoprefixer');
let csso = require('gulp-csso');
let htmlmin = require('gulp-htmlmin');
let uglify = require('gulp-uglify');
//let imgmin = require('gulp-imagemin');
// let njkRender = require('gulp-nunjucks-render');
// let prettify = require('gulp-html-prettify');

function copyHtml() {
    return gulp.src('src/index.html')
        .pipe(htmlmin())
        .pipe(gulp.dest('./'))
}

function doWatch() {
    gulp.watch('src/index.html', copyHtml).on('change', browserSync.reload)
    gulp.watch('src/scss/*.scss', scssToCss).on('change', browserSync.reload)
    gulp.watch('src/css/*.css', changeCss).on('change', browserSync.reload)
    gulp.watch('src/js/*.js', uglifyJs).on('change', browserSync.reload)
    gulp.watch('src/js/*.js', copyLibsJs).on('change', browserSync.reload)

    // gulp.watch('src/njk/**/*.njk', ['nunjucks'])
}

function serve() {
    browserSync.init({
        server: './'
    });
    // watch();
}

function scssToCss() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream())
}

function changeCss() {
    return gulp.src('src/css/*.css')
        .pipe(autoprefixer())
        .pipe(csso())
        .pipe(gulp.dest('build/css'))
}

function uglifyJs() {
    return gulp.src('src/js/*js')
        .pipe(uglify())

}



function copyLibsJs() {
    return gulp.src([
            'node_modules/jquery/dist/jquery.min.js',
            'src/js/*.js'
        ])
        .pipe(gulp.dest('build/js'))
}

function replaceImg() {
    return gulp.src('src/img/*')
        .pipe(gulp.dest('build/img/'))
}

// function njkHtml() {
//     return gulp.src('src/njk/*/*.njk')
//         .pipe(njkRender())
//         .pipe(prettify({
//             indent_size: 4 // размер отступа - 4 пробела
//         }))
//         .pipe(gulp.dest('build/index.html'))
// };


exports.build = gulp.series(copyHtml, scssToCss, copyLibsJs, changeCss, uglifyJs, replaceImg);
exports.watch = doWatch;
// exports.sass = scssToCss;
// exports.csso = changeCss;
// exports.autoprefixer = changeCss;
exports.serve = serve;
// exports.htmlmin = copyHtml;
// exports.htmlmin = copyHtml;
// exports.uglify = uglifyJs;